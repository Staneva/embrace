# Embrace Empatica

Web application used for test purpose (correct design implementation and proper backend data display).

**Necessary scripts to run the application:**

`**npm install**`
will install all node modules used in the project.

`**npm start**`
will start the server on which is our application (port 9000)

`**npm run watch-scss**`
if changes in the styles are required, the script has to be running to convert the SASS styles to CSS

Application URL - **localhost:9000**

**Test user credentials:**

username: **john@doe.com**

pass: **1234**