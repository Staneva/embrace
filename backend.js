var express = require("express");

var bodyParser = require('body-parser');

var app = express();

var path = require('path');

var users = {
  1: {
    id: 1,
    firstName: "John",
    lastName: "Doe",
    email: "john@doe.com",
    pass: "1234"
  }
};

var orders = {
  1: {
    id: 1,
    ref: "#ord-2018-a993bee3",
    status: "paid",
    tracking: {
      carrier: "UPS",
      trackingCode: "DAJA91930102NDAKKS0",
      status: "in_transit"
    },
    items: [
      {
        sku: "emb-mb-s",
        name: "Embrace Watch - Stretchable Band Black",
        amount: 249
      }
    ],
    discounts: [
      {
        name: "Christmas 2018 - 10% OFF",
        type: "percent",
        value: 10
      }
    ]
  },
  2: {
    id: 2,
    ref: "#ord-2018-b6012cc8",
    status: "paid",
    tracking: null,
    items: [
      {
        sku: "emb-bb-s",
        name: "Embrace Watch - Stretchable Band Blue",
        amount: 249
      },
      {
        sku: "emb-mb-s",
        name: "Embrace Watch - Stretchable Band Black",
        amount: 249
      }
    ],
    discounts: [
      {
        name: "2x1 Embrace",
        type: "amount",
        value: 249
      }
    ]
  }
};

var userOrders = { orders: [orders[1], orders[2]] };

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(express.static('.'));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname+'/src/index.html'));
});

app.get("/info", function(req, res) {
  res.json({
    version: "1.0.0",
    timestamp: new Date()
  });
});

app.post("/login", function(req, res) {
    for (var key in users) {
        if(req.body.email == users[key].email && req.body.pass == users[key].pass) {
            res.send({userId: key, logged: 'success'});
        } else {
            res.send({userId: key, logged: 'error'});
        }
    }
    // res.json({
    //   id: 1,
    //   state: 'success'
    // });
});

app.get("/users/:id", function(req, res) {
  res.json(users[+req.params["id"]]);
});

app.get("/users/:id/orders", function(req, res) {
  res.json(userOrders);
});

app.delete("/orders/:id", function(req, res) {
  res.json({
    orderId: +req.params["id"],
    status: "cancelled",
    order: orders[+req.params["id"]]
  });
});

console.log("Server is starting at localhost:9000");
app.listen(9000);
