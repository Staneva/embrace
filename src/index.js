// show user orders panel
document.getElementById('buy-now').onclick = function(){
    document.getElementById('user-orders').classList.add('active');
};
// hide user orders panel
document.getElementById('hide-user-orders').onclick = function(){
    document.getElementById('user-orders').classList.remove('active');
};

// directly display user orders if the user is already logged in
if(localStorage.getItem("loggedUser") !== null) {
    displayUserData(localStorage.getItem("loggedUser"));
};
// clear storage and refresh page on user log out
function userLogOut() {
    localStorage.removeItem("loggedUser");
    location.reload();
}

// on login
// - check if the user is existing
// - keep the user in local storage
// - dispaly his/hers orders
// - display messages on wrong user credentials or connection with backend error
function login() {
    var loginEmail = document.getElementById('login-email').value;
    var loginPass = document.getElementById('login-pass').value;
    var loginReq = new XMLHttpRequest();
    loginReq.open("POST", '/login', true);
    loginReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    loginReq.send('email='+loginEmail+'&pass='+loginPass);
    loginReq.responseType = 'json';
    loginReq.onreadystatechange = function () {
        if (loginReq.readyState === 4) {
           if(loginReq.response.logged === 'success') {
              localStorage.setItem("loggedUser", loginReq.response.userId);
              displayUserData(loginReq.response.userId);
           } else if(loginReq.response.logged === 'error') {
              document.getElementById('login-error').innerHTML = 'wrong email or password';
           } else {
              document.getElementById('login-error').innerHTML = 'connection error';
           }
        }
    };
};

// general function displaying user name and orders
function displayUserData(id) {
    document.getElementById('login-orders').innerHTML =
    '<div id="logged-user-orders">'+
        '<h2 class="user-name" id="user-name">'+ displayUserName(id) +'</h2>'+
        '<div class="link-effect log-out-link" onclick="userLogOut();">Log out</div>'+
        '<div id="orders-display">'+ displayUserOrders(id) +'</div>'
    '</div>';
};

// call user name from backend
function displayUserName(id) {
    let getUserReq = new XMLHttpRequest();
    getUserReq.open('GET', '/users/'+id );
    getUserReq.responseType = 'json';
    getUserReq.send();
    getUserReq.onreadystatechange = function() {
        if (getUserReq.readyState === 4) {
           document.getElementById('user-name').innerHTML = 'Welcome <span>' + getUserReq.response.firstName+' '+getUserReq.response.lastName +'</span>';
        };
    };
};

// call user orders from backend
function displayUserOrders(id) {
    let getOrdersReq = new XMLHttpRequest();
    getOrdersReq.open('GET', '/users/'+ id +'/orders' );
    getOrdersReq.responseType = 'json';
    getOrdersReq.send();
    getOrdersReq.onreadystatechange = function() {
        if (getOrdersReq.readyState === 4) {
            var orders = getOrdersReq.response.orders;
            var displayOrders = '';
            for(var i=0; i < orders.length; i++) {
                displayOrders += '<div class="order-wrapper">';

                displayOrders +=
                    '<div class="order-desc">'+
                        '<span>'+ orders[i].id +'</span>'+
                        '<span>'+ orders[i].ref +'</span>'+
                        '<span class="status">'+ orders[i].status +'</span>'+
                    '</div>';
                if(orders[i].tracking) {
                    displayOrders +=
                    '<div class="order-tracking">'+
                        '<span>'+ orders[i].tracking.carrier +'</span>'+
                        '<span>'+ orders[i].tracking.status +' | '+ orders[i].tracking.trackingCode +'</span>'+
                    '</div>'
                } else {
                    displayOrders += '<div class="order-tracking">No tracking information</div>';
                };
                displayOrders += displayItemsAndDisounts(orders[i].items, orders[i].discounts);

                displayOrders += '</div>';
            };
            document.getElementById('orders-display').innerHTML = displayOrders;
        };
    };
};

// function for listing items in each order, displaying discounts and calculating total amount
function  displayItemsAndDisounts(items, discounts) {
    var itemsAndDiscounts = '';
    var subtotalPrice = 0;
    var totalPrice = 0;
    var discountValue = '';
    for(var i=0; i < items.length; i++) {
        itemsAndDiscounts +=
        '<div class="order-item">'+
            '<span class="order-item-name">'+
                '<span class="order-item-sku">'+ items[i].sku +'</span>'+
                items[i].name +
            '</span>'+
            '<span class="order-item-amount">'+
                items[i].amount +
            ' €</span>'+
        '</div>';

        subtotalPrice += items[i].amount;
    };
    totalPrice = subtotalPrice;
    itemsAndDiscounts += '<div class="order-subtotal">subtotal: <span>'+ subtotalPrice.toFixed(2) +' €</span></div>';

    if(discounts !== null) {
        for(var d=0; d < discounts.length; d++) {
            discountValue = (discounts[d].type == 'percent') ? totalPrice*discounts[d].value/100 : discounts[d].value;
            totalPrice -= discountValue;
            itemsAndDiscounts +=
            '<div class="order-discount">'+
                discounts[d].name + ': <span>-'+ discountValue.toFixed(2) +' €</span>'+
            '</div>';
        };
    };

    itemsAndDiscounts += '<div class="order-total">total: <span>'+ totalPrice.toFixed(2) +' €</span></div>';

    return itemsAndDiscounts;
};
